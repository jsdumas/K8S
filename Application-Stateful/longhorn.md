## Objectif

Dans cette mise en pratique, nous allons mettre en place *Longhorn*, une solution de stockage de type block provenant de Rancher. Longhorn est également un projet de la CNCF.

![Longhorn Architecture](./images/longhorn-0.png)

## Environnement

Pour cet exercice, nous utiliserons un cluster de 3 nodes.

## Déploiement de Longhorn

Utiliser la commande suivante pour déployer Longhorn sur votre cluster.

```
kubectl apply -f https://raw.githubusercontent.com/longhorn/longhorn/v1.2.4/deploy/longhorn.yaml
```

Différents composants seront alors créés:

```
namespace/longhorn-system created
serviceaccount/longhorn-service-account created
clusterrole.rbac.authorization.k8s.io/longhorn-role created
clusterrolebinding.rbac.authorization.k8s.io/longhorn-bind created
customresourcedefinition.apiextensions.k8s.io/engines.longhorn.io created
customresourcedefinition.apiextensions.k8s.io/replicas.longhorn.io created
customresourcedefinition.apiextensions.k8s.io/settings.longhorn.io created
customresourcedefinition.apiextensions.k8s.io/volumes.longhorn.io created
customresourcedefinition.apiextensions.k8s.io/engineimages.longhorn.io created
customresourcedefinition.apiextensions.k8s.io/nodes.longhorn.io created
customresourcedefinition.apiextensions.k8s.io/instancemanagers.longhorn.io created
customresourcedefinition.apiextensions.k8s.io/sharemanagers.longhorn.io created
customresourcedefinition.apiextensions.k8s.io/backingimages.longhorn.io created
customresourcedefinition.apiextensions.k8s.io/backingimagemanagers.longhorn.io created
customresourcedefinition.apiextensions.k8s.io/backingimagedatasources.longhorn.io created
customresourcedefinition.apiextensions.k8s.io/backuptargets.longhorn.io created
customresourcedefinition.apiextensions.k8s.io/backupvolumes.longhorn.io created
customresourcedefinition.apiextensions.k8s.io/backups.longhorn.io created
customresourcedefinition.apiextensions.k8s.io/recurringjobs.longhorn.io created
configmap/longhorn-default-setting created
Warning: policy/v1beta1 PodSecurityPolicy is deprecated in v1.21+, unavailable in v1.25+
podsecuritypolicy.policy/longhorn-psp created
role.rbac.authorization.k8s.io/longhorn-psp-role created
rolebinding.rbac.authorization.k8s.io/longhorn-psp-binding created
configmap/longhorn-storageclass created
daemonset.apps/longhorn-manager created
service/longhorn-backend created
service/longhorn-engine-manager created
service/longhorn-replica-manager created
deployment.apps/longhorn-ui created
service/longhorn-frontend created
deployment.apps/longhorn-driver-deployer created
```

Après quelques dizaines de secondes vous pourrez vérifier que les Pods déployés par Longhorn tournent dans le namespace *longhorn-system*:

```
$ kubectl -n longhorn-system get po
NAME                                        READY   STATUS    RESTARTS      AGE
csi-attacher-6454556647-2mf2m               1/1     Running   0             41s
csi-attacher-6454556647-492nt               1/1     Running   0             41s
csi-attacher-6454556647-tkwfh               1/1     Running   0             41s
csi-provisioner-869bdc4b79-5nf55            1/1     Running   0             41s
csi-provisioner-869bdc4b79-bp9k7            1/1     Running   0             41s
csi-provisioner-869bdc4b79-s6zsk            1/1     Running   0             41s
csi-resizer-6d8cf5f99f-phsg9                1/1     Running   0             40s
csi-resizer-6d8cf5f99f-pvpck                1/1     Running   0             40s
csi-resizer-6d8cf5f99f-t544c                1/1     Running   0             40s
csi-snapshotter-588457fcdf-2lzff            1/1     Running   0             39s
csi-snapshotter-588457fcdf-tg787            1/1     Running   0             39s
csi-snapshotter-588457fcdf-xpxkj            1/1     Running   0             39s
engine-image-ei-4dbdb778-6x5rq              1/1     Running   0             48s
engine-image-ei-4dbdb778-859md              1/1     Running   0             48s
engine-image-ei-4dbdb778-j5cvk              1/1     Running   0             48s
instance-manager-e-423fde08                 1/1     Running   0             48s
instance-manager-e-ebc67432                 1/1     Running   0             47s
instance-manager-e-f242b1b9                 1/1     Running   0             47s
instance-manager-r-6d572683                 1/1     Running   0             46s
instance-manager-r-751e0f20                 1/1     Running   0             46s
instance-manager-r-cfc05178                 1/1     Running   0             48s
longhorn-csi-plugin-67vdx                   2/2     Running   0             39s
longhorn-csi-plugin-7znft                   2/2     Running   0             39s
longhorn-csi-plugin-w5qvb                   2/2     Running   0             39s
longhorn-driver-deployer-7c85dc8c69-zlv6c   1/1     Running   0             71s
longhorn-manager-bdc77                      1/1     Running   1 (50s ago)   73s
longhorn-manager-fdwj2                      1/1     Running   1 (50s ago)   73s
longhorn-manager-vqc8f                      1/1     Running   0             73s
longhorn-ui-6dcd69998-jhxrw                 1/1     Running   0             72s
```

Une *StorageClass* a également été mise en place:

```
$ kubectl get sc
NAME       PROVISIONER          RECLAIMPOLICY   VOLUMEBINDINGMODE   ALLOWVOLUMEEXPANSION   AGE
longhorn   driver.longhorn.io   Delete          Immediate           true                   4m56s
```

Note: si votre cluster ne contient pas déjà une StorageClass par défaut, vous pouvez utiliser la commande suivante afin d'utiliser celle créée par Longhorn:

```
kubectl patch storageclass longhorn -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
```

## Accès à l'interface web

Utilisez la commande suivante pour exposer l'interface web de Longhorn via un *port-forward*:

```
kubectl port-forward -n longhorn-system service/longhorn-frontend 8080:80
```

![Longhorn UI](./images/longhorn-1.png)

## Création d'un volume

La spécification suivante définit un *PersistentVolumeClaim* basé sur la *StorageClass* créée précédemment:

```
$  cat << EOF | kubectl apply -f -
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mongo-pvc
spec:
  storageClassName: longhorn
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 5G
EOF
```

Vous pouvez ensuite voir qu'un PVC et un PV ont été créés:

```
$ kubectl get pvc,pv
NAME                             STATUS  VOLUME      CAPACITY  ACCESS MODES  STORAGECLASS   AGE
persistentvolumeclaim/mongo-pvc  Bound   pvc-b5b...  5Gi       RWO           longhorn       61s

NAME           CAPACITY  ACCESS MODES  RECLAIM POLICY  STATUS   CLAIM               STORAGECLASS   REASON   AGE
pv/pvc-b5b...  5Gi       RWO           Delete          Bound    default/mongo-pvc   longhorn                57s
```

Depuis l'interface web de Longhorn on peut alors voir qu'un volume a été crée.

![Longhorn UI](./images/longhorn-2.png)

Dans la suite, vous allez utiliser ce volume dans un Deployment.

## Utilisation dans un workload de base de données

Lancez le Deployment suivant, celui-ci utilise le PVC *mongo-pvc* créé précédemment.

```
$ cat << EOF | kubectl apply -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: db
spec:
  selector:
    matchLabels:
      app: db
  template:
    metadata:
      labels:
        app: db
    spec:
      containers:
      - image: mongo:4.0
        name: mongo
        ports:
        - containerPort: 27017
        volumeMounts:
        - name: mongo-persistent-storage
          mountPath: /data/db
      volumes:
      - name: mongo-persistent-storage
        persistentVolumeClaim:
          claimName: mongo-pvc          
EOF
```

Depuis l'interface web de Longhorn vous pourrez alors voir que le volume est maintenant disponible et actif.

![Longhorn UI](./images/longhorn-3.png)

Depuis le menu *Node*, vous pourrez également voir que les 5G du volume sont répliqués sur les différentes nodes.

![Longhorn UI](./images/longhorn-4.png)

## Cleanup

Supprimez les ressources que vous avez créées:

```
kubectl delete deploy db
kubectl delete pvc mongo-pvc
```

La désintallation de Longhorn se passe en 2 étapes:

- lancez le job de désintallation et attendez qu'il se termine
```
kubectl create -f https://raw.githubusercontent.com/longhorn/longhorn/v1.2.4/uninstall/uninstall.yaml
kubectl get job/longhorn-uninstall -n default -w
```

- supprimez les autres composants
```
kubectl delete -f https://raw.githubusercontent.com/longhorn/longhorn/v1.2.4/deploy/longhorn.yaml
kubectl delete -f https://raw.githubusercontent.com/longhorn/longhorn/v1.2.4/uninstall/uninstall.yaml
```