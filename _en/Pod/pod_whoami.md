In this exercise, you will run your first Pod.

## Exercise

### 1. Create the Pod's specification

Create a file named *whoami.yaml* that specifies a Pod with the following characteristics:
- name: *whoami*
- container's image: *containous/whoami*

### 2. Launch the Pod

### 3. Test

Get the existing Pods and make sure the Pod named *whoami* is listed.

### 4. Pod's details

Get the Pod's details and find the information indicating it's IP address.

### 5. Access the application

Using *kubectl port-forward* send a request to the application running in the *whoami* Pod.

### 6. Delete the Pod

---

## Correction

### 1. Create the Pod's specification

The Pod's specification is the following one:

```
apiVersion: v1             
kind: Pod                  
metadata:
  name: whoami
spec:
  containers:
  - name: whoami
    image: containous/whoami
```

### 2. Launch the Pod

The Pod can be created with the following command:

```
kubectl apply -f whoami.yaml
```

### 3. Test

The following command list the existing Pods:

```
kubectl get pods
```

The Pod *whoami* should appear as *Running*:

```
NAME      READY     STATUS    RESTARTS   AGE
whoami    1/1       Running   0          14s
...
```

Note: *pod* (singular) or *po* can also be used as shortcuts of *pods*

```
$ kubectl get pod
NAME      READY     STATUS    RESTARTS   AGE
whoami    1/1       Running   0          16s

$ kubectl get po
NAME      READY     STATUS    RESTARTS   AGE
whoami    1/1       Running   0          22s
```

### 4. Pod's details

The Pod's details can be retreived with the following command:

```
kubectl describe pod whoami
```

You'll get an output similar to this one (in this example, the Pod's IP address is *192.168.117.160*):

```
Name:         whoami
Namespace:    default
Priority:     0
Node:         pool-0c6fd-ertzi/194.182.169.185
Start Time:   Mon, 21 Mar 2022 20:27:00 +0100
Labels:       <none>
Annotations:  cni.projectcalico.org/containerID: bf1482441187f92742b1485b6497e92af2b28398f9c2eaa4dc21776f5213af83
              cni.projectcalico.org/podIP: 192.168.117.160/32
              cni.projectcalico.org/podIPs: 192.168.117.160/32
              kubernetes.io/psp: privileged
Status:       Running
IP:           192.168.117.160
IPs:
  IP:  192.168.117.160
Containers:
  whoami:
    Container ID:   containerd://6b53f5f5945cef71c8fd40fc15edcd61a2682f092ea84ad3be08c9d59f448c68
    Image:          containous/whoami
    Image ID:       docker.io/containous/whoami@sha256:7d6a3c8f91470a23ef380320609ee6e69ac68d20bc804f3a1c6065fb56cfa34e
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Mon, 21 Mar 2022 20:27:04 +0100
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-k5t8q (ro)
Conditions:
  Type              Status
  Initialized       True
  Ready             True
  ContainersReady   True
  PodScheduled      True
Volumes:
  kube-api-access-k5t8q:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  60s   default-scheduler  Successfully assigned default/whoami to pool-0c6fd-ertzi
  Normal  Pulling    59s   kubelet            Pulling image "containous/whoami"
  Normal  Pulled     56s   kubelet            Successfully pulled image "containous/whoami" in 2.857153773s
  Normal  Created    56s   kubelet            Created container whoami
  Normal  Started    56s   kubelet            Started container whoami
```

Note: the following commands can also be used to describe the Pod *whoami*:
- kubectl describe pods whoami
- kubectl describe po whoami
- kubectl describe pods/whoami
- kubectl describe pod/whoami
- kubectl describe po/whoami

The whole Pod's specification can be retrieved with the following command using the *-o yaml* flag:

```
kubectl get po/whoami -o yaml
```

You'll get a result like the one below:

```
apiVersion: v1
kind: Pod
metadata:
  annotations:
    cni.projectcalico.org/containerID: bf1482441187f92742b1485b6497e92af2b28398f9c2eaa4dc21776f5213af83
    cni.projectcalico.org/podIP: 192.168.117.160/32
    cni.projectcalico.org/podIPs: 192.168.117.160/32
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","kind":"Pod","metadata":{"annotations":{},"name":"whoami","namespace":"default"},"spec":{"containers":[{"image":"containous/whoami","name":"whoami"}]}}
    kubernetes.io/psp: privileged
  creationTimestamp: "2022-03-21T19:27:00Z"
  name: whoami
  namespace: default
  resourceVersion: "1792248914"
  uid: cb52024e-5d56-414e-9547-bdce7b9b7ff8
spec:
  containers:
  - image: containous/whoami
    imagePullPolicy: Always
    name: whoami
    resources: {}
    terminationMessagePath: /dev/termination-log
    terminationMessagePolicy: File
    volumeMounts:
    - mountPath: /var/run/secrets/kubernetes.io/serviceaccount
      name: kube-api-access-k5t8q
      readOnly: true
  dnsPolicy: ClusterFirst
  enableServiceLinks: true
  nodeName: pool-0c6fd-ertzi
  preemptionPolicy: PreemptLowerPriority
  priority: 0
  restartPolicy: Always
  schedulerName: default-scheduler
  securityContext: {}
  serviceAccount: default
  serviceAccountName: default
  terminationGracePeriodSeconds: 30
  tolerations:
  - effect: NoExecute
    key: node.kubernetes.io/not-ready
    operator: Exists
    tolerationSeconds: 300
  - effect: NoExecute
    key: node.kubernetes.io/unreachable
    operator: Exists
    tolerationSeconds: 300
  volumes:
  - name: kube-api-access-k5t8q
    projected:
      defaultMode: 420
      sources:
      - serviceAccountToken:
          expirationSeconds: 3607
          path: token
      - configMap:
          items:
          - key: ca.crt
            path: ca.crt
          name: kube-root-ca.crt
      - downwardAPI:
          items:
          - fieldRef:
              apiVersion: v1
              fieldPath: metadata.namespace
            path: namespace
status:
  conditions:
  - lastProbeTime: null
    lastTransitionTime: "2022-03-21T19:27:00Z"
    status: "True"
    type: Initialized
  - lastProbeTime: null
    lastTransitionTime: "2022-03-21T19:27:04Z"
    status: "True"
    type: Ready
  - lastProbeTime: null
    lastTransitionTime: "2022-03-21T19:27:04Z"
    status: "True"
    type: ContainersReady
  - lastProbeTime: null
    lastTransitionTime: "2022-03-21T19:27:00Z"
    status: "True"
    type: PodScheduled
  containerStatuses:
  - containerID: containerd://6b53f5f5945cef71c8fd40fc15edcd61a2682f092ea84ad3be08c9d59f448c68
    image: docker.io/containous/whoami:latest
    imageID: docker.io/containous/whoami@sha256:7d6a3c8f91470a23ef380320609ee6e69ac68d20bc804f3a1c6065fb56cfa34e
    lastState: {}
    name: whoami
    ready: true
    restartCount: 0
    started: true
    state:
      running:
        startedAt: "2022-03-21T19:27:04Z"
  hostIP: 194.182.169.185
  phase: Running
  podIP: 192.168.117.160
  podIPs:
  - ip: 192.168.117.160
  qosClass: BestEffort
  startTime: "2022-03-21T19:27:00Z
```

The Pod's IP address can be retrieved in *.status.podIP*

### 5. Access the application

Run the following command to open a local port mapped to the Pod's container port:

```
kubectl port-forward whoami 8888:80
```

From your web browser, open the URL [http://localhost:8888](http://localhost:8888), you should get a reply similar to the following one:

```
Hostname: whoami
IP: 127.0.0.1
IP: ::1
IP: 192.168.117.160
IP: fe80::b866:55ff:fe79:3cf2
RemoteAddr: 127.0.0.1:58882
GET / HTTP/1.1
Host: localhost:8888
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Accept-Encoding: gzip, deflate, br
Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7
Cache-Control: max-age=0
Connection: keep-alive
If-None-Match: "5d52db33-264"
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: none
Sec-Fetch-User: ?1
Sec-Gpc: 1
Upgrade-Insecure-Requests: 1
```

### 6. Delete the Pod

The Pod can be deleted with the following command:

```
kubectl delete po/whoami
```
