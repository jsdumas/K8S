In this exercise you will create a Pod with 2 containers to launch Wordpress and its MySQL database.

## Exercise

### 1. Create the Pods's specification

Create a file named *wordpress_pod.yaml* that specifies a Pod with the following characteristics:
- name: *wp*
- first container:
  - named *wordpress*
  - based on the image *wordpress:4.9-apache*
  - defining the environment variable named *WORDPRESS_DB_PASSWORD* with the value *mysqlpwd* (see note below)
  - defining the environment variable named *WORDPRESS_DB_HOST* with the value *127.0.0.1* (see note below)
- a second container:
  - named *mysql*
  - based on the image *mysql:5.7*
  - defining the environment variable named *MYSQL_ROOT_PASSWORD* with the value *mysqlpwd* (see note below)

note: to define environment variables for a Pod's container's specification, you can use the following format:

```
...
containers:
- name: CONTAINER_NAME
  image: CONTAINER_IMAGE
  env:
  - name: VARIABLE_NAME
    value: VARIABLE_VALUE
```

### 2. Run the Pod

### 3. Check the Pod's status and get its IP address

### 4. Access the application using the *port-forward* subcommand

### 5. Delete the Pod

---

## Correction

### 1. Create the specification

The Pod's specification is the following one:

```
apiVersion: v1
kind: Pod
metadata:
  name: wp
spec:
  containers:
  - image: wordpress:4.9-apache
    name: wordpress
    env:
    - name: WORDPRESS_DB_PASSWORD
      value: mysqlpwd
    - name: WORDPRESS_DB_HOST
      value: 127.0.0.1
  - image: mysql:5.7
    name: mysql
    env:
    - name: MYSQL_ROOT_PASSWORD
      value: mysqlpwd
```

Note: the Pod's specification can also include a volume to persist the data outside of the *mysql* container's filesystem

```
apiVersion: v1
kind: Pod
metadata:
  name: wp
spec:
  containers:
  - image: wordpress:4.9-apache
    name: wordpress
    env:
    - name: WORDPRESS_DB_PASSWORD
      value: mysqlpwd
    - name: WORDPRESS_DB_HOST
      value: 127.0.0.1
  - image: mysql:5.7
    name: mysql
    env:
    - name: MYSQL_ROOT_PASSWORD
      value: mysqlpwd
    volumeMounts:
    - name: data
      mountPath: /var/lib/mysql
  volumes:
  - name: data
    emptyDir: {}
```

### 2. Run the Pod using *kubectl*

The Pod can be launched with the following command:

```
kubectl apply -f wordpress_pod.yaml
```

### 3. Check the Pod's status and get it's IP address

The following command get the status of the *wp* Pod:

```
kubectl get po/wp
```

For a couple of seconds, the Pod will be in the *ContainerCreating* status (while the images are downloaded from the DockerHub):

```
NAME      READY     STATUS              RESTARTS   AGE
wp        0/2       ContainerCreating   0          49s
```

Then the Pod will appear in the *Running* status:

```
NAME      READY     STATUS    RESTARTS   AGE
wp        2/2       Running   0          2m
```

### 4. Access the application using the *port-forward* subcommand

Run the following command to open a local port mapped to the Wordpress container's port:

```
kubectl port-forward wp 8080:80
```

From your web browser, open the URL [http://localhost:8080](http://localhost:8080).

Go on and configure Wordpress:

![Workpress](./images/wordpress1.png)
![Workpress](./images/wordpress2.png)
![Workpress](./images/wordpress3.png)
![Workpress](./images/wordpress4.png)
![Workpress](./images/wordpress5.png)

### 5. Delete the Pod

The Pod can be deleted with the following command:

```
kubectl delete po/wp
```
